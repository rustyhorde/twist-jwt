//! Decode a JWT.
use {Algorithm, Header, TokenData, TwistJwtResult};
use base64;
use error::TwistJwt;
use serde::{Deserialize, Serialize};
use serde_json;
use verify;

/// Decode the given JWT into the relevant parts.
pub fn jwt<T>(token: &str, secret: &[u8], alg: Algorithm) -> TwistJwtResult<TokenData<T>>
    where T: Serialize + Deserialize + Clone
{
    let mut parts = token.split('.');

    match (parts.next(), parts.next(), parts.next(), parts.next()) {
        (Some(header), Some(claims), Some(signature), None) => {
            if let Ok(true) = verify::verify(header, claims, signature, secret, alg) {
                let header_vec = base64::decode(header)?;
                let header: Header = serde_json::from_slice(&header_vec)?;

                if header.alg != alg {
                    return Err(TwistJwt::InvalidToken);
                }

                let claim_vec = base64::decode(claims)?;
                let claims: T = serde_json::from_slice(&claim_vec)?;

                Ok(TokenData {
                       header: header,
                       claims: claims,
                   })
            } else {
                Err(TwistJwt::InvalidToken)
            }
        }
        _ => Err(TwistJwt::InvalidToken),
    }
}

#[cfg(test)]
mod test {
    use Algorithm;
    use decode;
    use test::{Claims, HS256};

    #[test]
    fn default_decode() {
        match decode::jwt::<Claims>(HS256, "secret".as_bytes(), Algorithm::HS256) {
            Ok(td) => {
                let claims = td.claims();
                assert!(claims.sub == "1234567890");
                assert!(claims.name == "John Doe");
                assert!(claims.admin);
            }
            Err(_) => assert!(false),
        }
    }

    #[test]
    fn hs384_decode() {}
}
