//! twist server side extension.
use slog::Logger;

/// The jwt server-side extension configuration.
#[derive(Default)]
pub struct Jwt {
    /// Is this extension enabled?
    enabled: bool,
    /// slog stdout `Logger`
    stdout: Option<Logger>,
    /// slog stderr `Logger`
    stderr: Option<Logger>,
    /// The JWT.
    token: Vec<u8>,
}

impl Jwt {
    /// Set the `enabled` flag.
    pub fn set_enabled(&mut self, enabled: bool) -> &mut Jwt {
        self.enabled = enabled;
        self
    }

    /// Set the `token` value.
    // TODO: Remove this when decode is implemented.
    pub fn set_token(&mut self, token: &[u8]) -> &mut Jwt {
        self.token = token.into();
        self
    }

    /// Add a stdout slog `Logger` to this protocol.
    pub fn stdout(&mut self, logger: Logger) -> &mut Jwt {
        let stdout = logger.new(o!("extension" => "jwt", "module" => "server"));
        self.stdout = Some(stdout);
        self
    }

    /// Add a stderr slog `Logger` to this protocol.
    pub fn stderr(&mut self, logger: Logger) -> &mut Jwt {
        let stderr = logger.new(o!("extension" => "jwt", "module" => "server"));
        self.stderr = Some(stderr);
        self
    }
}
