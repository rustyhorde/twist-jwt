//! Encode a JWT
use {Header, TwistJwtResult};
use base64::{self, URL_SAFE_NO_PAD};
use serde::{Deserialize, Serialize};
use serde_json;
use sign;

/// Encode the given header, claims, and secret as a JWT.
pub fn jwt<T>(header: &Header, claims: &T, secret: &[u8]) -> TwistJwtResult<String>
    where T: Serialize + Deserialize + Clone
{
    let header_json = serde_json::to_string(header)?;
    let claims_json = serde_json::to_string(claims)?;
    let encoded_header = base64::encode_config(header_json.as_bytes(), URL_SAFE_NO_PAD);
    let encoded_claims = base64::encode_config(claims_json.as_bytes(), URL_SAFE_NO_PAD);
    let payload = [encoded_header, encoded_claims].join(".");
    let signature = sign::jwt(&payload, secret, header.alg)?;
    Ok([payload, signature].join("."))
}

#[cfg(test)]
mod test {
    use {Algorithm, Header};
    use encode;
    use test::{Claims, HS256, HS384, HS512, PS256_2048, PS256_4096, PS384_2048, PS384_4096,
               PS512_2048, PS512_4096, RS256_2048, RS256_4096, RS384_2048, RS384_4096, RS512_2048,
               RS512_4096};

    enum KeyBits {
        TwoZeroFourEight,
        FourZeroNineSix,
    }

    fn encode_with_secret(alg: Algorithm, actual: &str) {
        encode(alg, actual, "secret".as_bytes());
    }

    fn encode_with_path(alg: Algorithm, actual: &str, bits: KeyBits) {
        match bits {
            KeyBits::TwoZeroFourEight => {
                encode(alg, actual, "tests/twist-jwt-test-2048.der".as_bytes());
            }
            KeyBits::FourZeroNineSix => {
                encode(alg, actual, "tests/twist-jwt-test-4096.der".as_bytes());
            }
        }
    }

    fn encode(alg: Algorithm, expected: &str, secret_or_path: &[u8]) {
        let mut header: Header = Default::default();
        header.set_alg(alg);
        let claims = Claims {
            sub: "1234567890".to_string(),
            name: "John Doe".to_string(),
            admin: true,
        };
        match encode::jwt(&header, &claims, secret_or_path) {
            Ok(actual) => {
                match alg {
                    // The RSAPSS algos are non-deterministic, so the signature piece varies.
                    // Check the header and payload only.
                    Algorithm::PS256 | Algorithm::PS384 | Algorithm::PS512 => {
                        let ehp = expected.splitn(3, '.');
                        let ahp = actual.splitn(3, '.');
                        for ((idx, a), b) in ehp.enumerate().zip(ahp) {
                            if idx == 0 || idx == 1 {
                                assert!(a == b);
                            }
                        }
                    }
                    _ => assert!(actual == expected),
                }
            }
            Err(e) => {
                err!("{}", e);
                assert!(false)
            }
        }
    }

    #[test]
    fn hs256_encode() {
        encode_with_secret(Algorithm::HS256, HS256);
    }

    #[test]
    fn hs384_encode() {
        encode_with_secret(Algorithm::HS384, HS384);
    }

    #[test]
    fn hs512_encode() {
        encode_with_secret(Algorithm::HS512, HS512);
    }

    #[test]
    fn ps256_2048_bits_key_encode() {
        encode_with_path(Algorithm::PS256, PS256_2048, KeyBits::TwoZeroFourEight);
    }

    #[test]
    fn ps256_4096_bits_key_encode() {
        encode_with_path(Algorithm::PS256, PS256_4096, KeyBits::FourZeroNineSix);
    }

    #[test]
    fn ps384_2048_bits_key_encode() {
        encode_with_path(Algorithm::PS384, PS384_2048, KeyBits::TwoZeroFourEight);
    }

    #[test]
    fn ps384_4096_bits_key_encode() {
        encode_with_path(Algorithm::PS384, PS384_4096, KeyBits::FourZeroNineSix);
    }

    #[test]
    fn ps512_2048_bits_key_encode() {
        encode_with_path(Algorithm::PS512, PS512_2048, KeyBits::TwoZeroFourEight);
    }

    #[test]
    fn ps512_4096_bits_key_encode() {
        encode_with_path(Algorithm::PS512, PS512_4096, KeyBits::FourZeroNineSix);
    }

    #[test]
    fn rs256_2048_bits_key_encode() {
        encode_with_path(Algorithm::RS256, RS256_2048, KeyBits::TwoZeroFourEight);
    }

    #[test]
    fn rs256_4096_bits_key_encode() {
        encode_with_path(Algorithm::RS256, RS256_4096, KeyBits::FourZeroNineSix);
    }

    #[test]
    fn rs384_2048_bits_key_encode() {
        encode_with_path(Algorithm::RS384, RS384_2048, KeyBits::TwoZeroFourEight);
    }

    #[test]
    fn rs384_4096_bits_key_encode() {
        encode_with_path(Algorithm::RS384, RS384_4096, KeyBits::FourZeroNineSix);
    }

    #[test]
    fn rs512_2048_bits_key_encode() {
        encode_with_path(Algorithm::RS512, RS512_2048, KeyBits::TwoZeroFourEight);
    }

    #[test]
    fn rs512_4096_bits_key_encode() {
        encode_with_path(Algorithm::RS512, RS512_4096, KeyBits::FourZeroNineSix);
    }
}
