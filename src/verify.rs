//! Verify a JWT.
use {Algorithm, TwistJwtResult};
use chrono::{DateTime, UTC};
use sign;

/// Trait used for Registered Claim validation.
pub trait HasRegistered {
    /// Identifies the principal that issued the JWT.
    fn iss(&self) -> Option<String>;
    /// Identifies the principal that is the subject of the JWT.
    fn sub(&self) -> Option<String>;
    /// Identifies the recipients that the JWT is intended for.
    fn aud(&self) -> Option<String>;
    /// Identifies the expiration time on or after which the JWT MUST NOT be accepted for
    /// processing.
    fn exp(&self) -> Option<DateTime<UTC>>;
    /// Identifies the time before which the JWT MUST NOT be accepted for processing.
    fn nbf(&self) -> Option<DateTime<UTC>>;
    /// Identifies the time at which the JWT was issued.
    fn iat(&self) -> Option<DateTime<UTC>>;
    /// Provides a unique identifier for the JWT.
    fn jti(&self) -> Option<String>;
}

bitflags! {
    /// Used to tell the verify_claims function which claims to verify.
    pub flags Claim: u8 {
        /// Validate the `iss` claim.
        const ISS = 0b00000001,
        /// Validate the `sub` claim.
        const SUB = 0b00000010,
        /// Validate the `aud` claim.
        const AUD = 0b00000100,
        /// Validate the `exp` claim.
        const EXP = 0b00001000,
        /// Validate the `nbf` claim.
        const NBF = 0b00010000,
        /// Validate the `iat` claim.
        const IAT = 0b00100000,
        /// Validate the `jti` claim.
        const JTI = 0b01000000,
        /// All registered claims.
        const ALL = ISS.bits | SUB.bits | AUD.bits | EXP.bits | NBF.bits | IAT.bits | JTI.bits,
    }
}

/// Verify the given token parts against the signature, secret, and algorithm.
pub fn verify(header: &str,
              claims: &str,
              tok_signature: &str,
              secret: &[u8],
              alg: Algorithm)
              -> TwistJwtResult<bool> {
    let payload = [header, claims].join(".");
    let sig = sign::jwt(&payload, secret, alg)?;
    if sig.len() != tok_signature.len() {
        return Ok(false);
    }
    for (a, b) in tok_signature.chars().zip(sig.chars()) {
        if a != b {
            return Ok(false);
        }
    }
    Ok(true)
}

macro_rules! verify_reg(
    ($i:ident) => (verify_reg!($i, &str););
    ($i:ident, $t:ty) => (
        /// Verify the unverified claims against the given claim value.
        pub fn $i<T: HasRegistered>(unverified: &T, $i: $t) -> bool {
            if let Some(unverified) = HasRegistered::$i(unverified) {
                if unverified != $i {
                    return false;
                }
            } else {
                return false;
            }

            true
        }
    );
);

verify_reg!(iss);
verify_reg!(sub);
verify_reg!(aud);
verify_reg!(exp, DateTime<UTC>);
verify_reg!(nbf, DateTime<UTC>);
verify_reg!(iat, DateTime<UTC>);
verify_reg!(jti);

macro_rules! verify_claim_str(
    ($claim:ident, $claimid:ident, $unverified:expr, $verified:expr, $flags:expr) => (
        if $flags.contains($claim) {
            if let Some(verified_arg) = HasRegistered::$claimid($verified) {
                if !$claimid($unverified, &verified_arg) {
                    return false;
                }
            } else {
                return false;
            }
        }
    );
);

macro_rules! verify_claim_dt(
    ($claim:ident, $claimid:ident, $unverified:expr, $verified:expr, $flags:expr) => (
        if $flags.contains($claim) {
            if let Some(verified_arg) = HasRegistered::$claimid($verified) {
                if !$claimid($unverified, verified_arg) {
                    return false;
                }
            } else {
                return false;
            }
        }
    );
);

/// Verify the claims identified by `flag` from `unverified` against `verified`.
pub fn claims<T>(unverified: &T, verified: &T, flags: Claim) -> bool
    where T: HasRegistered
{
    verify_claim_str!(ISS, iss, unverified, verified, flags);
    verify_claim_str!(SUB, sub, unverified, verified, flags);
    verify_claim_str!(AUD, aud, unverified, verified, flags);
    verify_claim_dt!(EXP, exp, unverified, verified, flags);
    verify_claim_dt!(NBF, nbf, unverified, verified, flags);
    verify_claim_dt!(IAT, iat, unverified, verified, flags);
    verify_claim_str!(JTI, jti, unverified, verified, flags);

    true
}

#[cfg(test)]
mod test {
    use Algorithm;
    use chrono::{DateTime, UTC};
    use decode;
    use test::ISS_ONLY;
    use verify;

    #[test]
    fn iss_only() {
        use verify::ISS;

        #[derive(Clone, Deserialize, Serialize)]
        struct IssClaim {
            iss: String,
        };

        impl super::HasRegistered for IssClaim {
            fn iss(&self) -> Option<String> {
                Some(self.iss.clone())
            }

            fn sub(&self) -> Option<String> {
                None
            }

            fn aud(&self) -> Option<String> {
                None
            }

            fn exp(&self) -> Option<DateTime<UTC>> {
                None
            }

            fn nbf(&self) -> Option<DateTime<UTC>> {
                None
            }

            fn iat(&self) -> Option<DateTime<UTC>> {
                None
            }

            fn jti(&self) -> Option<String> {
                None
            }
        }

        match decode::jwt::<IssClaim>(ISS_ONLY, "secret".as_bytes(), Algorithm::HS256) {
            Ok(td) => {
                let claims = td.claims();
                assert!(claims.iss == "ellmak.io");
                assert!(verify::iss(&claims, "ellmak.io"));
                assert!(verify::claims(&claims, &IssClaim { iss: "ellmak.io".to_string() }, ISS));
            }
            Err(_) => assert!(false),
        }
    }
}
